const Sequelize = require('sequelize');
const constitute = require('constitute');
const DatabaseService = require('./databaseService');
const databaseService = constitute(DatabaseService);
/**
 * DatabaseService
 */
class RoadDefinitionDao {
  constructor() {
    this.sequelize = databaseService.sequelize;
    this.RoadDefinition = this.sequelize.import('../../models/roaddeinition');
    this.RoadPosition = this.sequelize.import('../../models/roadposition');
  }

  async getRoadByRoadNameWithNativeSQL(bindValues) {
    //return await this.sequelize.query('SELECT * FROM road_definitions');
    return await this.sequelize.query('SELECT * FROM road_definitions WHERE road_name = $roadName',
      { bind: bindValues, type: this.sequelize.QueryTypes.SELECT }
    );
  }

  async createRoad() {
    const RoadDefinition = this.sequelize.import('../../models/roaddeinition');
    return this.sequelize.transaction(function (t) {
      return RoadDefinition.create({
        roadName: 'Abraham',
        state: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      }, { transaction: t }).then(function (roadDefinition) {
        // Woops, the query was successful but we still want to roll back!
        throw new Error();
      });
    });
  }


  async getRoadAndPostionByRoadName(roadName) {
    this.RoadDefinition.hasMany(this.RoadPosition, { as: 'positions', foreignKey: 'roadName', sourceKey: 'roadName' });
    this.RoadPosition.belongsTo(this.RoadDefinition, { as: 'definition', foreignKey: 'roadName', targetKey: 'roadName' });
    let result;
    if (roadName) {
      result = await this.RoadDefinition.findOne({
        where: {
          roadName: roadName,
        },
      });
    }
    else {
      result = await this.RoadDefinition.findAll({
        where: {
        },
      }, { order: 'id' });
    }    
    return result;
  }
  async getRoadByRoadName(roadName) {

    if (roadName) {
      return await this.RoadDefinition.findOne({
        where: {
          roadName: roadName,
        },
      });
    }
    else {
      return await this.RoadDefinition.findAll({
        where: {
        },
      }, { order: 'id' });
    }
  }

  async updateRoadDefinition(updateValues, roadName) {
    await this.RoadDefinition.update(updateValues, { where: { roadName: roadName } });
  }
  testConnection() {
    console.log('test dao');
    this.sequelize
      .authenticate()
      .then(() => {
        console.log('Connection has been established successfully.');
      })
      .catch(err => {
        console.error('Unable to connect to the database:', err);
      });
  }

}

module.exports = RoadDefinitionDao;

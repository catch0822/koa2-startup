const Sequelize = require('sequelize') ;

/**
 * DruidService
 */
 class DatabaseService {
  constructor() {
    console.log('create data base conn');
    // console.log('constructor new sequelize');
    this.sequelize = new Sequelize('node_db', 'root', 'root', {
      host: '192.168.21.213',
      port: 3306,
    //  logging: false,
      dialect: 'mysql',
      pool: {
        max: 5,
        min: 0,
        idle: 10000,
      },
    });
  }
}
module.exports = DatabaseService;
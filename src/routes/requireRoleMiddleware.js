const log4js = require('log4js');
const log = log4js.getLogger('Middleware');
log.level = 'debug';

//特定router套用的middleware
const requireRole = (role) => {
  return async function(ctx, next) {
    try {
      log.debug(`session.role: 
      ${ctx.session.role}`);
      if ((role.indexOf(ctx.session.role) > -1)) {
        next();
        
      } else {
        ctx.body = 'Access Denied!!!';
      }
    } catch (ex) {
      ctx.body = 'Access Denied!!!';
    }
  };
};

//所有router套用的middleware
const httpLogger = () => {
  return async function (ctx, next) {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    log.debug(`HTTP ${ctx.method} ${ctx.url} - ${ms}ms`);
  };
};

module.exports.requireRole = requireRole;
module.exports.httpLogger = httpLogger;
  
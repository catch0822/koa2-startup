
const Router = require('koa-router');
const constitute = require('constitute');
const RoadDefinitionDao = require('../db/roadDefinition.dao');
const roadDefinitionDao = constitute(RoadDefinitionDao);


const roadRouter = Router();
roadRouter.get('/roads', async (ctx) => {
    try {
        // const result = await roadDefinitionDao.getRoadByRoadName('Main Line');
        const result = await roadDefinitionDao.getRoadByRoadName(null);
        console.log('content', result);
        ctx.body = result;
        console.log('road log');
    } catch (e) {
        console.error(e);
    }
});

roadRouter.get('/roads/create', async (ctx) => {
    try {
        // const result = await roadDefinitionDao.getRoadByRoadName('Main Line');
        const result = await roadDefinitionDao.createRoad();
        console.log('content', result);
        ctx.body = result;
        console.log('road log');
    } catch (e) {
        console.error(e);
    }
});

roadRouter.get('/roads/allPostions', async (ctx) => {
    try {
        // const result = await roadDefinitionDao.getRoadByRoadName('Main Line');        
        const result = await roadDefinitionDao.getRoadAndPostionByRoadName('King Line');
        console.log('content', result.dataValues);
        const pos = await result.getPositions({
            where: {
                type: 'coneType',
            },
        });
        console.log('==========  position', pos);
        ctx.body = pos;
        console.log('road log');
    } catch (e) {
        console.error(e);
    }
});

roadRouter.get('/roads/native', async (ctx) => {
    try {
        // const result = await roadDefinitionDao.getRoadByRoadName('Main Line');
        const bindValue = { roadName: 'Main Line' };
        const result = await roadDefinitionDao.getRoadByRoadNameWithNativeSQL(bindValue);
        console.log('content', result);
        ctx.body = result;
        console.log('road log');
    } catch (e) {
        console.error(e);
    }
});

roadRouter.get('/roads', async (ctx) => {
    try {
        // const result = await roadDefinitionDao.getRoadByRoadName('Main Line');
        const result = await roadDefinitionDao.getRoadByRoadName(null);
        console.log('content', result);
        ctx.body = result;
        console.log('road log');
    } catch (e) {
        console.error(e);
    }
});

roadRouter.get('/roads/update', async (ctx) => {
    try {
        const roadName = ctx.query.road;
        // console.log(ctx.query.road);
        const result = await roadDefinitionDao.getRoadByRoadName(roadName);
        result.dataValues.updatedAt = new Date();
        await roadDefinitionDao.updateRoadDefinition(result.dataValues, roadName);
        ctx.body = 'OK';
    } catch (e) {
        console.error(e);
    }
});
module.exports = roadRouter;

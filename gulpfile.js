const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const livereload = require('gulp-livereload');

gulp.task('watch', () => {
  livereload.listen();
  gulp.watch([
    'src/conf/**/*.js',
    'src/routes/**/*.js',
    'src/models/**/*.js',
    'src/controllers/**/*.js',
    'src/**/*.js'
  ]);
  nodemon({
    script: './bin/www',
    ext: 'js'
  });
});

gulp.task('default', ['watch']);
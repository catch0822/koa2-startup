const roadRouter = require('./src/road/road.controller');
const Koa = require('koa');
const Router = require('koa-router');
const log4js = require('log4js');
const log4js_extend = require('log4js-extend');
const bodyParser = require('koa-bodyparser');
const logger = require('koa-logger');
const session = require('koa-session');
const { requireRole, httpLogger } = require('./src/routes/requireRoleMiddleware');


log4js_extend(log4js, {
  path: __dirname,
  format: 'at @name (@file:@line:@column)'
});
const log = log4js.getLogger('Wei\'s App.js');
log.level = 'debug';

const CONFIG = {
  key: 'wei', /** (string) cookie key (default is koa:sess) */
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 86400000,
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. default is false **/
};

const app = new Koa();
app.keys = ['weiSecret'];
app.use(session(CONFIG, app));
app.use(httpLogger());
const router = Router();

//HTTP GET
router.get('/', async (ctx) => {
  if (ctx.query.name != null) {
    if (ctx.query.name.toUpperCase() != 'WEI')
      ctx.throw(400);
  }
  ctx.body = 'Hello WEI';
});

router.get('/setSession', async (ctx) => {
  ctx.query.role == null ? ctx.session.role = '1' : ctx.session.role = ctx.query.role;
  ctx.body = 'SET SESSION Done.';
});

router.get('/getUserInfo', requireRole(['1', '2']), async (ctx) => {
  let { name: username } = ctx.query;
  log.debug(`post name: ${username}`);
  ctx.body = `Hello ${username}`;
});

app.use(logger());
app.use(bodyParser());
app.use(router.routes());
app.use(roadRouter.routes());
app.listen(3000);
module.exports = app;
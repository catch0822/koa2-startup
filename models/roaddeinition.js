//import Sequelize from 'sequelize';
const Sequelize = require ('sequelize');
'use strict';
module.exports = (sequelize) => {
  let RoadDefinition = sequelize.define('road_definitions', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      field: 'id',
      type: Sequelize.INTEGER,
    },
    roadName: {
      allowNull: false,
      field: 'road_name',
      type: Sequelize.STRING(50),
    },
    state: {
      allowNull: false,
      field: 'state',
      defaultValue: 1,
      // 0:disable, 1:enable, 2:reset
      type: Sequelize.INTEGER,
    },
    createdAt: {
      allowNull: false,
      field: 'created_at',
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      field: 'updated_at',
      type: Sequelize.DATE,
    },
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      },
    },
  });
  return RoadDefinition;
};

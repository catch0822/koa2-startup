'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('road_definitions', [{
      road_name: 'King Line',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      road_name: 'Main Line',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      road_name: 'Kaida Granny Avenue',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      road_name: 'Fifth Avenue',
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      road_name: 'Rocky Mountain Railway',
      created_at: new Date(),
      updated_at: new Date(),
    },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
  },
};

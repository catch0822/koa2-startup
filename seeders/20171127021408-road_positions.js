'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('road_positions', [
      {
        // 1
        position_x: 12.75,
        position_y: 10.30,
        type: 'coneRoute',
        road_name: 'King Line',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        // 2
        position_x: 23.75,
        position_y: 10.30,
        type: 'coneRoute',
        road_name: 'King Line',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        // 3
        position_x: 34.76,
        position_y: 10.30,
        type: 'coneRoute',
        road_name: 'King Line',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        // 4
        position_x: 45.70,
        position_y: 10.30,
        type: 'coneRoute',
        road_name: 'King Line',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        // 5
        position_x: 56.71,
        position_y: 10.30,
        road_name: 'King Line',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        // 6
        position_x: 67.71,
        position_y: 10.30,
        road_name: 'King Line',
        created_at: new Date(),
        updated_at: new Date(),
      },

    ], {});
  },

  down: (queryInterface, Sequelize) => {

  },
};

'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('road_positions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        field: 'id',
        type: Sequelize.INTEGER,
      },
      tagId: {
        allowNull: true,
        field: 'tag_id',
        type: Sequelize.STRING(50),
      },
      positionX: {
        allowNull: true,
        field: 'position_x',
        type: Sequelize.FLOAT,
      },
      positionY: {
        allowNull: true,
        field: 'position_y',
        type: Sequelize.FLOAT,
      },
      type: {
        allowNull: true,
        field: 'type',
        type: Sequelize.STRING(50),
      },
      state: {
        allowNull: false,
        field: 'state',
        defaultValue: 1,
        type: Sequelize.BOOLEAN,
      },
      roadName: {
        allowNull: true,
        field: 'road_name',
        type: Sequelize.STRING(50),
      },    
      createdAt: {
        allowNull: false,
        field: 'created_at',
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        field: 'updated_at',
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('road_positions');
  },
};


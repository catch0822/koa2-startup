'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('road_definitions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        field: 'id',
        type: Sequelize.INTEGER,
      },
      roadName: {
        allowNull: false,
        field: 'road_name',
        type: Sequelize.STRING(50),
      },
      state: {
        allowNull: false,
        field: 'state',
        defaultValue: 1,
        // 0:disable, 1:enable
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        field: 'created_at',
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        field: 'updated_at',
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('road_definitions');
  },
};
